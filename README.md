## Symfony-Webkit

# Introduction
This is an app build dummy which helps you to port your browser Symfony2|whatever PHP application to a standard desktop app.

App is based on Nightrain (https://github.com/naetech/nightrain), special thanks to this wonderful webkit

# How to Install

1. clone project dummy to the desired directory

```sh 
$ git clone git@bitbucket.org:mowlwurf/symfony-webkit.git <your_new_app_name> 
```

2. create/copy your project into contents folder
```sh
$ cd ./dummy.app/Contents/
$ composer create-project symfony/framework-standard-edition <my_project_name>
```
3. create index.php in your web directory
```php
$ <?php
$
$ include('app.php'); # optional, just used for sf projects
$
```
# Configuration

1. dummy.app/Contents/MacOS/settings.ini
```ini
$ ; full path or the relative path to the php executable
$ php_path = ./php/php      #  this app comes with own php binary, but you can use your own too
$ ; full path or the relative path to the directory from where
$ ; the server will load your php files
$ webroot = ../YourProject/web/     # dummy path to server web directory, where the index.php should be located
$ ; default port from where php dummy launches its web server
$ ; make sure your firewall is not blocking this port
$ port = 8000
$ ; default application width
$ width = 800
$ ; default application height
$ height = 600
$ ; default window status (true or false)
$ maximized = False
$ ; start the application in full screen (true or false)
$ ; you can press 'f11' to toggle full screen as well while
$ ; the application is running
$ ; this feature is for windows only
$ fullscreen = False
$ ; amount of time to wait before showing the application gui
$ ; this is useful if there is a delay in launching the web server
$ ; or the firewall on your computer is interfering
$ wait_time = 1
```
2. dummy.app/Contents/MacOS/php

This directory contains the default built-in php binary, you can copy your own here, or define the installed client version (f.e. /usr/bin/php)